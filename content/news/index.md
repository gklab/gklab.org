---
description: 
draft: false
layout: single-pageOnly
show_title_as_headline: true
title: News
---

### Fall 2024


- The lab continues to grow! Welcome [Anita](/people/anita), who is joining us after finishing up their PhD at Duke to work on plant--microbe interactions in longleaf pine savannas. 

### Summer 2024

- It's an exciting end to summer as we welcome [Rachana](/people/rachana) and [Edmarie](/people/edmarie) to the lab! Can't wait for the fun science to come over the years! 

- Gaurav had the pleasure to attend the [SABER](https://saberbio.wildapricot.org/event-5603329/Registration) (Society for the Advancement of Biology Education Research) annual meeting at the University of Minnesota. He shared a poster about [EcoEvoApps](https://padlet.com/ayesilaltay/saber-24-posters-on-padlet-giwmq8cwfvjc1lp/wish/KxJvagePxVA8ZAg0) and was a co-author on another about the [UNIDE network](https://padlet.com/ayesilaltay/saber-24-posters-on-padlet-giwmq8cwfvjc1lp/wish/E851Q0ExpMVbZVAb). It was great to be back on the beautiful UMN campus and energizing to meet the BER community!

- Welcome to the lab, [Rohit Jha](/people/rohit)! Rohit will be joining us starting this Fall and will work on his dissertation research in collaboration with Gaurav and [Daijiang Li](https://https://www.dlilab.com/), as Daijiang's lab moves from LSU. 

- June: Welcome back [Alexander Philips](/people/alexander)! Alex will be working with us over the next year as a lab manager/research assistant, while also working towards his Master's degree in Biotechnology. 

- June: Welcome to [Xavier Jackson](/people/xavier), who will be joining us for the summer (and hopefully beyond!) to help out with various aspects of the Lifeplan and Dragnet projects. 

- May: Gaurav visited [iDiv](https://www.idiv.de/en/spsf) for the third working group meeting of the sPSF working group. What a privilege to work with this group of friends on fun and important questions!

<img src="https://www.idiv.de/fileadmin/content/_processed_/f/e/csm_sPSF_3_group_picture_da1c59f78e.jpg" class="center-photo" alt="archbold biological station" style="width:400px;"/>


### Spring 2024

It's been a busy semester! Here are some highlights from the last few months:

- May: Gaurav's first paper from LSU was published at the American Journal of Botany. This paper provides an overview of the mathematical frameworks that empirical researchers can use to quantify how microbes shape plant coexistence. Read the paper [here](https://bsapubs.onlinelibrary.wiley.com/doi/epdf/10.1002/ajb2.16316) :memo:

- April: We're thrilled that [Rachana Rao](https://www.ncf-india.org/author/1581980/rachana-rao) will be joining us this Fall as a PhD student! While she waits for grad school to start, Rachana has also started a new field experiment on the role of soil microbes in mediating seedling dynamics at the interface between coffee plantations and natural forests :seedling:

- April: Gaurav visited [Archbold Biological Station](https://www.instagram.com/p/C5V349oOHej/) as a Distinguished Seminar Speaker. Thank you Aaron for the invitation, and hope to visit you again before too long :fire:

<img src="img/archbold.jpg" class="center-photo" alt="archbold biological station" style="width:400px;"/>

- March: Richard and Gaurav conducted the first round of soil sampling to start experimenting on plants native to Louisiana. Ultimately we decided to shelve the experiment for now, but collecting the soil provided an opportunity for Richard to practice the protocols :four_leaf_clover:

<img src="img/richard-dragnet.jpg" class="center-photo" alt="archbold biological station" style="width:400px;"/>


### Fall 2023

<details>
<summary> Click here to read about our exciting first semester at LSU!</summary>

**December**: 

- Welcome to [Ameya Kherde](/people/ameya/), who is joining the lab as our first remote internship student! Over the coming month, Ameya will be learning how to process microbiome sequencing data.

- Congrats to all lab members for completing a busy and eventful semester! We are looking forward to moving into our new (permanent) lab space in LSA 363 next semester, to welcome a few more undergrad researchers, and to get going with some fun new science! 

**November**: 

- Big congrats to Dr. Richard Ita on his official PhD conferral from the University of Uyo! :tada: We'll have to call him Dr. Dr. by the end of his time at LSU 
- New paper published from the [REC network](https://qubeshub.org/publications/4350/1), with whom Gaurav has had the pleasure of working over the past few years (including the Fall Think Tank event this month!) :memo:. 
  - Morton, T. *et al.*. 2023 [Re-Envisioning the Culture of Undergraduate Biology Education to Foster Black Student Success: A Clarion Call](https://www.lifescied.org/doi/epdf/10.1187/cbe.22-09-0175). *CBE-Life Sciences Education*.
  
- Thanks to various collaborators, Gaurav has also been a part of four other manuscript submissions this month, making this a busy and exciting time. Can't wait to share the results with everyone!
  
**September**: 

- [Milayna](/people/milayna), [Alex](/people/alexander), and [Swati](/people/swati) join the lab as undergraduate researchers :seedling:

**August**: 
- [Gaurav](/people/gaurav) and [Richard](/people/richard) moved to Baton Rouge and are eager to set up the lab at LSU :seedling:
 <details>
