---
description: 
draft: false
layout: single-pageOnly
show_title_as_headline: true
title: 
---

**Thanks for your interest in joining our group.**

<mark>I am recruiting students, postdocs, and other lab members to grow our  research group at Lousiana State University.</mark> 

I am eager to work with students and postdocs from a wide range of backgrounds, and I am committed to changing the culture of ecology to be more inclusive of students and scholars from backgrounds that are largely excluded from our field.

Please [get in touch](email:gkandlikar@lsu.edu) if you might be interested in working with the lab![^contact]

--------------

<u><i>Undergraduate students</u></i>: The lab is always open to taking in undergraduate students who are curious about the science of ecology, and are dedicated to growing as scientists. Students can work for research credits and/or for hourly pay (depending on funding and project). 

<mark>**If you are interested in joining our group**, please fill out [this form](gklab-undergrad-interest-form.docx) and [email it to Gaurav](emailto:gkandlikar@lsu.edu) with the subject like "GK Lab Undergrad Research interest".</mark>


--------------

<u><i>Graduate students</u></i>: I am open to working with students on a wide range of ecological questions, field systems, and approaches. All graduate students in my lab can expect a baseline of funding (salary plus research funding) from a combination of departmental Teaching Assistantships and lab-supported Research Assistantships. I also work closely with students to secure additional funding from external fellowships and grants.

<details  class="tab" 
    style="background: lavenderblush; 
    padding: 10px; 
    border: 1px solid lightgray; 
    margin: 5px;">
<summary>
<i><b>Graduate students who wish to start in Fall 2025 need to submit their application to LSU by mid-December 2024. Please click here for more details.</b></i>  </summary>
<br>
Please <a href="mailto:gkandlikar@lsu.edu">email me</a> by November 1 2024 if you think you might be interested in joining LSU as a grad student in Fall 2025. <b>Your email should include a specific paragraph explaining how our lab's research interests in plant community ecology, mathematical ecology, and/or pedagogy align with your goals.</b> You don't have to know exactly what research you want to conduct yourself - but I do want to know what about our lab's work has specifically caught your attention.

If our academic interests align, I would love to meet with you and answer any questions about your potential fit in my lab and at LSU, the logistics of applying to graduate school, my approach to mentoring graduate students, and whatever other doubts or ideas you might have. I can also connect you with current and previous LSU students who can talk about other aspects of life at and around LSU. 

For more details about graduate applications about LSU, please check out the <a href="https://www.lsu.edu/science/biosci/programs/graduate/prospective-students.php">Prospective Students</a> page on the department website. International applicants should also read through the <a href="https://www.lsu.edu/graduateschool/admissions/internationaladmissions.php">International Admissions</a> page. I will be a member of the Systematics, Ecology, and Evolution division - you can also take a look at <a href="https://www.lsu.edu/science/biosci/divisions/see.php">other faculty in this division</a>, who might be future committee members or co-advisors.

</details>

--------------

<u><i>Postdocs</i></u>: I will advertise for a postdoc to join the lab in 2024. In addition to this internally funded position, I am always eager to collaborate with potential postdocs on independent fellowships  proposals, e.g. through NSF, LSRF, or other agencies. 

---------------

**Lab values**

Our lab will have a deliberate dual focus on natural sciences and on critically evaluating and re-imagining how we do science.  Over time, this website will include an articulation of the values that will guide the lab. 


[^contact]: Contacting a potential faculty mentor can feel intimidating, especially if you are seeking out opportunities for undergraduate research or as a potential graduate student. [This post from Científico Latino](https://www.cientificolatino.com/post/contacting_prospectivesupervisor) provides great pointers on writing an initial contact email. Above all, please remember that if you are interested in joining my group after learning about my work, I will be as excited to meet with you as you will be about me!  And if you don't get a response from me within a week, please send a follow-up email. 