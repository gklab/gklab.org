---
description: Thank you to all the contributors!
draft: false
layout: standard
show_title_as_headline: true
title: About this site
---

Thank you to all the contributors who developed the themes to make this site possible. 


This site is made with the [Hugo Apero](https://github.com/hugo-apero/) theme using the [Blogdown](https://github.com/rstudio/blogdown) package. Hugo Apero builds on [Blogphoponic](https://github.com/formspree/blogophonic-hugo) and [Formspree](https://formspree.io). 

-----

## Contributors 

Thank you to all the folks who have contributed both technical and creative skills to this project:

+ [Desirée De Leon :giraffe:](http://desiree.rbind.io/) (designed 5 of the custom color themes, made illustrations for the workshop, and provided general aesthetic feedback along the way)

+ [Garrick Aden-Buie :mage:](https://www.garrickadenbuie.com/) (debugged headroom.js and lent his panelset.js code to the theme)

+ [Allison Horst :dog2:](https://www.allisonhorst.com/) (awesome illustrations of campfires, seedlings, and evergreens, as well as my R Markdown hedgehog mascot :hedgehog:)

+ [Maëlle Salmon :fishing_pole_and_fish:](https://masalmon.eu/) (help with features, and naming the theme [Hugo Apéro](https://hugo-apero.netlify.app/)!)

+ [Christophe Dervieux :crayon:](http://cderv.rbind.io/) (thinking through blogdown/Hugo intricacies and syntax highlighting)

+ [Yihui Xie :martial_arts_uniform:](https://yihui.org/) (for the blogdown package, getting me hooked on Hugo, and helping me with layout code inspired by his many Hugo themes)

+ [Athanasia Monika Mowinckel :purple_heart:](https://drmowinckels.io/) (for help finding :bug: and SASS support for making color themes work so much better :art:)

+ [Jannik Buhr :otter:](https://jmbuhr.de) (enabling math rendering with mathjax and katex)

+ [Garrick Aden-Buie, Silvia Canelón, Patricia Loto, and Shannon Pileggi](https://presentable-user2021.netlify.app/) (from whose shared github repository Gaurav got the sourcode for the People page)

And last but not least, Eric Anderson and the team at [Formspree](https://formspree.io/) for developing a Hugo theme with such great bones: <https://github.com/formspree/blogophonic-hugo>


----------------

## License

My [blog posts](/post/) are released under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).

<center>
<i class="fab fa-creative-commons fa-2x"></i><i class="fab fa-creative-commons-by fa-2x"></i><i class="fab fa-creative-commons-sa fa-2x"></i>
</center>