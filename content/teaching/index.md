---
description: Thank you to all the contributors!
draft: false
layout: standard
show_title_as_headline: true
title: Teaching
---

### Fall 2024

- Gaurav will teaching an upper-division undergrad course in Principles of Ecology. All course content is available at [this link](https://ecology.gklab.org).



### Fall 2023

- Gaurav is teaching an upper-division undergrad course in Principles of Ecology. All course content is available at [this link](https://ecology-2023.gklab.org).
