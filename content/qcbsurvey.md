---
description: 
draft: false
layout: single
show_title_as_headline: true
title: Biology students' impressions of math & programming 
date: 2024-07-16
category: pedagogy
---

**Thank you for your interest** in completing our survey regarding undergraduate biology students' impressions of mathematics and computer programming in biology! 

***If you are an undergrad student and are ready to take the survey, click on the button below!*** 

<a href="https://lsu.qualtrics.com/jfe/form/SV_dglerHguacCcdpA" target="_blank" class="button"  style="color: #FAF9F0;">Take the survey!</a>

If you would like more information about this project, keep reading below. 

### Why are we doing this?
The past two decades have seen a rapid rise in the ubiquity of quantitative and computational approaches in the life sciences. Biology education is evolving to keep up, with ever-growing efforts to integrate quantitative and computational skills into traditional biology curricula, and a blossoming of new undergraduate programs that sit at the intersection of biology, statistics, and computer science. However, without intentional interventions designed to foster equity and inclusion, there is a risk that this new paradigm in biology education exacerbates racial and gender disparities, which are even more extreme in mathematical and computational STEM fields than they currently are in the traditional life sciences. 

This survey aims to understand how undergraduate biology students perceive the potential value and costs of such quantitative/computational training in biology, as well as evaluating whether there are any demographic differences in these attitudes. We feel this is a critical first step towards mitigating potential gender, racial, or other disparities in quantitative and computational biology education. 

### About the survey

Our survey content is adapted from the [Math-Biology Values Inventory (MBVI)](https://www.lifescied.org/doi/full/10.1187/cbe.17-03-0043), a validated instrument developed by [Dr. Melissa Aikens](https://colsa.unh.edu/person/melissa-aikens)' group at the University of New Hampshire. The survey also asks students about their enthusiasm for taking biology courses that incorporate mathematics and/or programming components, as well as questions about students' demographic background and past STEM coursework. 

We expect that students can complete the survey within ~15 minutes. 

### IRB Approval

This project has been evaluated and approved by the Louisiana State University Institutional Review Board (#IRBAM-24-0641). 
