---
type: collection
date: "2021-01-25"
excerpt: Assorted formatting templates that Gaurav finds useful in his daily work
layout: single-series
publishDate: "2021-01-22"
show_author_byline: false
show_post_date: false
show_post_thumbnail: true
title: Templates
weight: 2
---


Here is a set of templates that Gaurav regularly uses. 

- Reference template for making Word documents from Rmd/Qmd  source: [link](custom-reference-doc.docx)

![](featured.jpg)
*Photo: Primrose (Oenothera sp.) and Desert Verbena in Anza Borrego Desert State Park, CA, in March 2023.*