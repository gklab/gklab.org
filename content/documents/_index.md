---
author: Gaurav Kandlikar
type: collection
cascade:
  layout: single-series
  sidebar:
    description: "Here you will find a collection of documents that we often refer to in the lab."
    text_contents_label: This week
    text_link_label: ""
    text_link_url: ""
    text_series_label: All documents
    title: Useful lab documents
layout: list-sidebar
show_author_byline: false
show_post_date: false
show_post_thumbnail: true
thumbnail_left: false
title: Lab documents
---
