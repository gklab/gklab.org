---
type: collection
date: "2021-01-25"
excerpt: Please fill out the enclosed document to officially request a letter of reference.
layout: single-series
publishDate: "2021-01-22"
show_author_byline: false
show_post_date: false
show_post_thumbnail: true
title: Requesting a letter of reference
weight: 2
---

If you would like to request Gaurav to write a supporting letter for an application you are submitting, please fill out and email me this document: [Link to Word file](LoR-information-file.docx). 


Please give me a **2-week heads up** for any letter. If there are extenuating circumstances (e.g. you find out last-minute about an opportunity you wish to apply to), email me anyway. 

Good luck with your application!

![](featured.jpg)
*Photo: Forest-grassland ecotone in the Western Ghats near Kadamane Tea Estate, Karnataka, India*. 