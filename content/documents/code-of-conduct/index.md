---
type: collection
date: "2021-01-25"
excerpt: We expect all lab members to adhere by this community-developed Code of Conduct.
layout: single-series
publishDate: "2021-01-22"
show_author_byline: false
show_post_date: false
show_post_thumbnail: true
title: Code of Conduct
weight: 1
---

Starting in Spring 2024, the lab will work together to establish a shared code of conduct. We will revisit this code of conduct annual to refresh our memories, and to make changes as needed.

![](featured.jpg)
*Photo of a butterfly visiting coneflowers, taken on a neighborhood walk in Columbia, MO.*