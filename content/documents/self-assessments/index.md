---
author: Gaurav Kandlikar
layout: "single-series"
type: "collection"
date: "2023-12-15"
excerpt: Every semester, we complete this self reflection and assessment exercise to ensure that our activities align with long-term goals.
show_author_byline: false
show_post_date: false
show_post_thumbnail: true
title: Self reflection and assessment
weight: 2
---

At the beginning of every semester (including summer session), we check in on our progress over the preceding few months, and plan for what is to come. 

The goals of this exercise are (i) to ensure that lab mentees and PI on the same page for short-term and long-term goals, (ii) to help you in setting goals and evaluating progress, (iii) to make sure you give yourself credit for your accomplishments and activities, and (iv) discuss ways to improve the lab culture. 

*Please set aside a couple of hours to complete this reflection process, and send it to me well ahead of our scheduled meeting time so that I can go through all you have written*. 

Download the guided reflection as a Word document here: [Self-reflection word document](self-reflection-and-assessment.docx).

If you prefer to complete the reflections in Markdown, the source is available here: [Self-reflection markdown file](_self-reflection.qmd).

------------

**Note**: I am deeply grateful to [Dr. Lauren Sullivan](https://www.sullivanplantecology.com/) for introducing me to these reflections. Lauren herself adopted the practice from [Dr. Allison Shaw](https://allisonkshaw.weebly.com/)'s lab before that. 

![](featured.jpg)
*Photo: A large herd of elk, many of whom appear to be completing their self-reflections. Photo taken near Sisu Ranch in Neskowin, OR.*