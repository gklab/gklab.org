---
type: collection
date: "2021-01-25"
excerpt: How can we reshape biology education in a way that ensures success of all students?
layout: single-series
publishDate: "2021-01-22"
show_author_byline: false
show_post_date: false
show_post_thumbnail: true
title: Inclusive and equitable biology education
weight: 3
---

Teaching is at the heart of what we do as University-based scientists, and we are committed to developing and implementing pedagogical strategies that center equity and inclusion in the classroom and other research settings. Specifically, we are committed to helping develop an inclusive pedagogy of quantitative biology education, which is a emerging as one of the most important themes of modern undergraduate biology education. We work towards this goal by developing new interactive tools to that make mathematical models more accessible to students (e.g. [EcoEvoApps](https://ecoevoapps.gitlab.io)). Gaurav also engages in this work in collaboration with the [UNIDE](https://unidecology.org/) and [REC](https://www.lifescied.org/doi/10.1187/cbe.22-09-0175) networks. 

### Ongoing pedagogy projects

- Developing tools for teaching mathematical models in ecology and evolution (read more [here](https://ecoevoapps.gitlab.io))
- Survey of biology students' attitudes of math & programming (read more [here](/qcb-survey))

![](featured.jpg)
*Photo of Clarkia bottae flowering in a growth chamber experiment*