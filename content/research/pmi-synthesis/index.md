---
type: collection
date: "2021-01-25"
excerpt: How do global change factors such as drought and nutrient enrichment reshape microbial controls on plant dynamics?
layout: single-series
publishDate: "2021-01-22"
show_author_byline: false
show_post_date: false
show_post_thumbnail: true
title: Plant-microbe interactions under global change
weight: 2
---

A central challenge in ecology is to predict how global environmental change will affect the processes that mediate the biodiversity dynamics of plant communities. One type of process that is already being affected is the feedback between plants and soil microbes -- a widespread mechanism that controls species coexistence, invasions, and succession. Along with wonderful colleagues in an international [working group](https://www.idiv.de/en/spsf.html) funded by sDiv, we are working to integrate mathematical models and  experimental data on plant--microbe interactions to build a more synthetic and predictive understanding of microbe-mediated plant coexistence under global change. 

![](spsf-minus-yadu-stan.jpg)
*Note: Photo of sPSF group members Gyuri Barabas, Meghna Krishnadas, Adriana Corrales, Gaurav Kandlikar, Po-Ju Ke, and Xinyi Yan at the Leipzig Botanical Gardens (Botanischer Garten) in June 2023. Not pictured: Yadugiri V.T. and Stan Harpole.*