---
author: Gaurav Kandlikar
type: collection
cascade:
  layout: single-series
  sidebar:
    description: "Our research is motivated by our curiosity about the dynamics of ecological systems, and by a deep desire to help address pressing environmental challenges. Please click below to read about the central themes of our work."
    text_link_label: ""
    text_link_url: ""
    text_series_label: Research Themes
    title: Research in our lab
layout: list-sidebar
show_author_byline: false
show_post_date: false
show_post_thumbnail: true
thumbnail_left: false
title: Research overview
---
