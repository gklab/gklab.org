---
type: collection
date: "2021-01-25"
excerpt: How do fragmentation-induced disruptions to plant-microbe interactions scale up to affect forest dynamics?
layout: single-series
publishDate: "2021-01-22"
show_author_byline: false
show_post_date: false
show_post_thumbnail: true
title: Seedling dynamics in tropical forest fragments
weight: 1
---

Habitat degradation is one of the main drivers of biodiversity loss  worldwide. One aspect of habitat degradation is that once-contiguous habitats have been separated into disconnected fragments, where the edges often have distinct environmental conditions and lower biodiversity compared to forest interiors. These altered environmental conditions can further disrupt biotic interactions, including plant interactions with soil-borne pathogens, mutualists, and other microbes. 

Ongoing work in our lab aims to address how these disruptions to plant-soil microbe interactions can scale up to affect biodiversity dynamics in fragmented forest landscapes. Specifically, Gaurav is collaborating with [Dr. Meghna Krishnadas](https://meghnakrishnadas.wixsite.com/forestecology/about-me) to evaluate how abiotic variation associated with habitat fragmentation shapes microbially-mediated plant coexistence among seedlings of tropical forest trees in the Western Ghats biodiversity hotspot. 

You can read more about our work in the British Ecological Society's Summer 2023 Magazine ([The Niche](https://www.britishecologicalsociety.org/wp-content/uploads/2023/06/BES_TheNiche_Summer_2023_pages.pdf); pg. 13). [These slides](gkandlikar-esa2023-kadumane.pdf) from Gaurav's talk at ESA 2023 also presents some preliminary results from our experiments. 

![](featured.jpg)
*Photo from Kadumane Tea Estate in Karnataka, India. The tropical forests in this landscape have been extensively reshaped by human activity, e.g. due to conversion of forest into tea plantations.*