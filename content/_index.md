---
# action_label: Read More &rarr;
action_link: /about
action_type: text
description: Welcome to Gaurav Kandlikar's research group at  <a href="https://www.lsu.edu/science/biosci/"><font color="#461d7c">Louisiana State University</font></a>. We are a community of scholars working to improve and share our understanding of ecological communities, and to making the world a better place while we do it. <br><br>Please take a look around this website to get a better sense of the people and projects in our group. We are always eager to [connect](emailto:gkandlikar@lsu.edu) over shared interests and ideas. <!--<br><br> <small><mark>***Note.** This website is under construction. In the mean time, please check out Gaurav's [personal page](https://gauravsk.gitlab.io)*</mark></small>-->
image_left: true
images:
- img/lsu-oaks.jpg
show_action_link: true
show_social_links: true
subtitle: 
text_align_left: true
title: Welcome
type: home
news: Lab News
---

** index doesn't contain a body, just front matter above.
See index.html in the layouts folder **
