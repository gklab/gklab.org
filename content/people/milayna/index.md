---
title: "Milayna Ulloa"
subtitle: "Undergraduate researcher"
pronouns: "she/her/hers"
type: people
show_post_image: true
weight: 3
author: "Gaurav Kandlikar"
draft: false
categories:
  - people
# layout options: single or single-sidebar
layout: single
---

**Hometown:** Ruston, LA

**Project:** Milayna is spearheading the GK Lab's Plot Points podcast. Read about the podcast [here]()!

**Why I like plant ecology:**  I love the opportunity to study and make an impact on something that affects everyone. Noticed or not, plant ecology affects our everyday lives.
