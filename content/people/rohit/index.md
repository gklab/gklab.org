---
title: "Rohit Jha"
subtitle: "Graduate researcher"
pronouns: "he/him/his"
type: people
show_post_image: true
weight: 2
draft: false
categories:
  - people
# layout options: single or single-sidebar
layout: single
---

**Hometown**: Rajbiraj, Nepal

**Research Interests:** I am broadly interested in biological invasions, altitudinal shifts of plant communities, plant-animal interactions, coupled human and natural systems, and biodiversity conservation.

**Projects:** One project that I would like to briefly mention is a citizen science-based phenology project. This work, which is also part of my PhD thesis, aims to gather phenology data of invasive plant species observed on citizen science platforms from their native and invaded sites. The goal is to correlate this data with climate variables to explore if species show different sensitivities to similar climates in their native and invaded sites. 

**Past experiences:** Grasslands and freshwater ecosystems are my areas of interest. My past work includes exploring the habitat preference and distribution of otters in the rivers of the lowlands of Nepal, studying the habitat-use patterns and behavior of a generalist grazer (blackbuck) in human-dominated landscapes, and working with fishermen communities to develop sustainable fishing practices to protect the last remaining population of river dolphins in Nepal. Find more about my work here: [Google Scholar](https://scholar.google.com/citations?user=TWXrBwcAAAAJ&hl=en).

**Let's connect** on [Twitter](https://x.com/HitRooh)!