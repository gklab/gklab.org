---
title: "Gaurav Kandlikar"
subtitle: "Assistant Professor and Lab PI"
pronouns: "he/him/his"
type: people
show_post_image: true
weight: 1
draft: false
categories:
  - people
# layout options: single or single-sidebar
layout: single
---

**Hometown**: Depending on the day/context... Mumbai or Hyderabad, India; Minneapolis, MN; Los Angeles, CA; or other places that feel like 'home'

**Past experiences:** I came to LSU after a PhD at UCLA and three years as a postdoc at the University of Missouri

**Research Interests:** My primary interests are in plant community ecology, pedagogy of biology, and in making science more open and inclusive. As lab PI, I also let my own curiosity be guided by that of the students and researchers around me. 

For more information about me, please check out my [CV](GSK_CV.pdf) or my [personal website](https://gauravsk.gitlab.io)

**Projects:** My ongoing projects are primarily focused on how soil microbes shape species coexistence and other processes in plant communities. Thanks in large  part due to a fantastic network of collaborators, I enjoy the distinct privilege of working across ecosystems ranging from grasslands to forests, and I especially enjoy integrating theory and data to do science. 