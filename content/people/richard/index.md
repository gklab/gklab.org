---
title: "Richard Ekeng Ita"
subtitle: "Graduate researcher"
pronouns: "he/him/his"
type: people
show_post_image: true
weight: 2
draft: false
categories:
  - people
# layout options: single or single-sidebar
layout: single
---

**Hometown**: Uyo, Nigeria

**Research Interests:** Plant-soil interactions, quantitative ecology, climate change, biodiversity conservation.

**Projects:** Richard is currently exploring ideas about plant-soil interactions and quantitative plant ecology to develop his PhD dissertation projects. 

**Past experiences:** Richard holds a PhD and MSc in Plant Ecology, and BSc. in Botany and Ecological Studies. Prior to joining LSU, Richard was a Lecturer in the Department of Biological Sciences, Ritman University, Ikot Ekpene, Nigeria.

Richard has led the following projects:
- Carbon stocks in natural and plantation forest ecosystems in Akwa Ibom State, Nigeria.
- Phytodiversity and Ecological Status in rural and urban lacustrine wetlands in Uyo, Akwa Ibom State, Nigeria