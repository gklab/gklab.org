---
title: "Xavier Jackson"
subtitle: "Undergraduate researcher"
pronouns: "he/him/his"
type: people
show_post_image: true
weight: 99
draft: false
categories:
  - people
# layout options: single or single-sidebar
layout: single
---

**Hometown**: Lewisville,TX

**Project**: Xavier is helping out with the Lifeplan project in Daijiang Li's lab, and is also helping process samples from the Baton Rouge Dragnet site


**Why I like plant ecology**: To me ecology is the ground work of ecosystems that hold some of my favorite species and animals. Having the opportunity to study how plants and animals interact within a habitat has always been an interest of mine