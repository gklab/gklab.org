---
title: "Edmarie Rivera-Sánchez"
subtitle: "LAGNiAppE Scholar"
pronouns: "she/her/hers"
type: people
show_post_image: true
weight: 3
draft: false
categories:
  - people
# layout options: single or single-sidebar
layout: single
---

**Hometown**: Florida, Puerto Rico

**Research Interests:** Host-microbe interactions, microbial ecology, fungal species, climate change and conservation biology

**Projects:** Soil microbial effects on plant eco-evolutionary dynamics under abiotic stress

**Past experiences:** Graduated in June 2024  with a BSc. in Biology with Emphasis on Environmental Microbiology from the University of Puerto Rico at Arecibo. As an undergraduate student, I participated on different research projects such as: 
- Analyzing and characterizing morphometry of fungi present in Leatherback sea turtle’s nests
- Screening Mucoromycota fungi isolates for bacterial symbionts
- Comparing and annotating genomes of Drosophila species


Let’s connect on [LinkedIn](https://www.linkedin.com/in/edmarie-rivera-sanchez-she-her-?utm_source=share&utm_campaign=share_via&utm_content=profile&utm_medium=ios_app) or [Twitter](https://x.com/edmarieriv5?s=21)! 
