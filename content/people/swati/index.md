---
title: "Swati Haldar"
subtitle: "Undergraduate researcher"
pronouns: "she/her/hers"
type: alum
show_post_image: true
weight: 3
draft: false
categories:
  - people
# layout options: single or single-sidebar
layout: single
---

**Hometown:** New Orleans, LA

**Project**: Swati is helping start up work on plant species interactions in coastal prairies

**Why I like plant ecology:**  I love seeing the bright colors of various plants and being able to cultivate them and observe their behavior from the very beginning. Along with learning about the unique traits of diverse plants and their surviving mechanism to flourish in a given environment, it is fun to discover their contribution to the development of their neighboring populations.


