---
title: "You?"
subtitle: "Undergraduate researcher/Graduate researcher/Postdoc"
pronouns: ""
type: people
show_post_image: true
weight: 99
draft: false
categories:
  - people
# layout options: single or single-sidebar
layout: single
---

We are eager to hear from prospective lab members! Please contact [Gaurav](email:gkandlikar@lsu.edu) with the following information:

- What draws you to our work?
- In what capacity would you like to join us? (Undergraduate student researcher, graduate student, postdoc, visiting scholar, etc.)
- What are your past experiences that you would like to build on by being in the lab? (Note: if you are an undergraduate student, it is totally OK for this to be your first experience in an ecology/research setting. But I would still love to hear about your relationship with ecology or nature more generally.)
- When would you like to join our group, inand how much time can you commit? 
