---
title: "Alexander Phillips"
subtitle: "Lab manager/Research technician"
pronouns: "he/him/his"
type: people
show_post_image: true
weight: 3
draft: false
categories:
  - people
# layout options: single or single-sidebar
layout: single
---

**Hometown**: New Orleans, LA

**Project**: Alex started working with us as an undergrad student in 2023, and is now working as a lab manager/research technician. In this role he will work on various ongoing projects and will also develop his own project focused on drought effects on plant--microbe interactions. 


**Why I like plant ecology**: One thing I like about plant ecology is learning how plants can help with local issues such as erosion and how we can help the native plant population thrive. I also enjoy learning about different species and broadening my understanding of plant ecology overall.
