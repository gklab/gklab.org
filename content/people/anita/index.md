---
title: "Anita Simha"
subtitle: "Postdoc"
pronouns: "they/them"
type: people
show_post_image: true
weight: 2
draft: false
categories:
  - people
# layout options: single or single-sidebar
layout: single
---

**Hometown**: I grew up mostly in Cary, North Carolina, and I claim Durham as home too

**Past experiences**: I came to LSU as a postdoctoral researcher after completing my PhD at Duke. I got my bachelor’s degree at UNC, so my basketball loyalties lie with the Tar Heels.

**Research interests**: I am interested in how ecological history shapes plant communities. I study both short-term legacies, like the effects of germination order on species interactions, and longer-term legacies, like the effects of past fire on plant community responses to future fire. I also explore ongoing legacies of colonialism, plantation slavery, and patriarchy in the field of ecology using insights from de-/postcolonial feminist science studies.

**Projects**: In the Kandlikar lab, I look forward to investigating how soil microbial legacies affect woody encroachment in fire-suppressed longleaf pine savanna.

Check out my website [anitasimha.com](https://anitasimha.com) for more information about me.