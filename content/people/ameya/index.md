---
title: "Ameya Kherde"
subtitle: "Undergraduate researcher"
pronouns: "He/Him/His"
type: alum
show_post_image: true
weight: 4
draft: false
categories:
  - alum
# layout options: single or single-sidebar
layout: single
---

**Hometown**: Nagpur, India  

**Project**: Ameya is helping analyze DNA metabarcoding data of soil bacterial and fungal communities from tallgrass prairies in the midwestern US. He is a remote intern, and is currently a student at IISER in Pune, India. 

**Why I like plant ecology**: I grew up on a steady diet of nature documentaries. They drove home the fact that no organism lives in isolation, but is in fact a part of a much larger interconnected network which through its countless moving parts and interactions shapes and sustains the organisms that make it.  I find this concept very beautiful and thus am trying my hand at ecology to appreciate it better